

$(document).ready(function(){

	var old_price_json = [];
	var new_price_json = [];
	var rowNum;

	function getNumberOfRows(){
		var num = document.getElementById('crypto-currency-list').rows.length;
		return num;
	}
	function setIcons(){
		var table = document.getElementById('crypto-currency-list');
		for(var i = 1; i < rowNum; i++){
			var firstCol = table.rows[i].cells[0];
			var symbol = firstCol.innerHTML;
			var icon = document.createElement('i');
			icon.className += 'cc '+symbol;
			firstCol.innerHTML = '';
			firstCol.appendChild(icon);
		}
	}
	function addPercentageSign(){
		var table = document.getElementById('crypto-currency-list');
		for(var i = 1; i < rowNum; i++){
			var col = table.rows[i].cells[4];
			col.innerHTML = col.innerHTML + '%';
		}
	}
	function savePrices(value){
		var table = document.getElementById('crypto-currency-list');
		if(value == false){
			old_price_json = [];
			for(var i = 1; i < rowNum; i++){
				var item = {};
				item ["id"] = table.rows[i].id;
        		item ["price"] = table.rows[i].cells[5].innerHTML;
        		old_price_json.push(item);
			}
		}else{
			new_price_json = [];
			for(var i = 1; i < rowNum; i++){
				var item = {};
				item ["id"] = table.rows[i].id;
        		item ["price"] = table.rows[i].cells[5].innerHTML;
        		new_price_json.push(item);
			}
		}
	}
	function checkPriceChange(){
		for(var i = 0; i<old_price_json.length; i++){
			var old_price = old_price_json[i].price;
			var old_id = old_price_json[i].id;
			for(var y = 0; y<new_price_json.length; y++){
				var new_price = new_price_json[y].price;
				var new_id = new_price_json[y].id;
				if(old_id == new_id){
					if(old_price != new_price){
						if(parseFloat(old_price) < parseFloat(new_price)){
							$('#'+new_id).addClass('flash-green');
						}else{
							$('#'+new_id).addClass('flash-red');
						}
					}
				}else{
					continue;
				}
			}
		}
	}

	$('#numOfCoins').on('input',function(){
		var value = parseFloat($(this).val());
		var price_input = $('#priceOfCoins');
		var modal = $('#tradeCryptoModal');
		var price = parseFloat(modal.find('h5').text().match(/\d+\.\d+/g));
		var final_price = value*price;
		price_input.val(final_price);
    });

    $('#priceOfCoins').on('input',function(){
		var value = parseFloat($(this).val());
		var price_input = $('#numOfCoins');
		var modal = $('#tradeCryptoModal');
		var price = parseFloat(modal.find('h5').text().match(/\d+\.\d+/g));
		var final_price = value/price;
		price_input.val(final_price);
    });

	var table = $('#crypto-currency-list').DataTable({
    	"ajax": {
    		"url": "https://api.coinmarketcap.com/v1/ticker/?limit=10",
    		"dataSrc": ""
  		},
  		"columns": [
            { "data": "symbol" },
            { "data": "name" },
            { "data": "symbol" },
            { "data": "market_cap_usd" },
            { "data": "percent_change_24h" },
            { "data": "price_usd" }
        ],
        "rowId": "id",
        "paging": false,
        "bInfo" : false,
        "searching": false,
        "initComplete": function(){
        	rowNum = getNumberOfRows();
        	setIcons();
        	addPercentageSign();
        	savePrices(false);	
        }
	});

	$('#crypto-currency-list tbody').on('click', 'tr', function () {
		var current = $(this);
        var coin_name = current.children('td').eq(1).text();
        var coin_price = parseFloat(current.children('td').eq(5).text());

        var modal = $('#tradeCryptoModal');

        modal.find('input').val('');
        modal.find('h5').text('You are buying '+coin_name+' coins for '+coin_price+' per coin !');

        modal.modal('show');
    } );

	setInterval( function () {
    	table.ajax.reload( function(){
    		rowNum = getNumberOfRows();
    		setIcons();
    		addPercentageSign();
    		savePrices(true);
    		checkPriceChange();
    		savePrices(false);
    	});
	}, 10000 );

});
